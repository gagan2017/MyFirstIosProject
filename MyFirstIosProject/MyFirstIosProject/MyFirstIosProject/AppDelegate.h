//
//  AppDelegate.h
//  MyFirstIosProject
//
//  Created by tarams on 04/01/17.
//  Copyright (c) 2017 gagan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
