//
//  main.m
//  MyFirstIosProject
//
//  Created by tarams on 04/01/17.
//  Copyright (c) 2017 gagan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
